﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ProductAPI.Models
{
    public class ProductContext : DbContext
    {
        // class constructor
        public ProductContext(DbContextOptions<ProductContext>options) : base(options)
        {

        }

        //dbset that returns the product's dbset
        public DbSet<Product> ProductItems { get; set; }
    }
    
    
}
