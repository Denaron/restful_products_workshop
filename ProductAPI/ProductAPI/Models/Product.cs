﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductAPI.Models
{
    public class Product
    {
        //Atributos da classe
        private int productID;
        private string productCode;
        private string name;
        private int quantity;
        private decimal price;

        //propriedades

        public int ProductID { get => productID; set => productID = value; }
        public string ProductCode { get => productCode; set => productCode = value; }
        public string Name { get => name; set => name = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public decimal Price { get => price; set => price = value; }


    }
}
