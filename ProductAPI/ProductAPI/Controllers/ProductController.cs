﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProductAPI.Models;


namespace ProductAPI.Controllers
{
    [Produces("application/json", "application/xml", "application/html")]
    [Route("api/products")]
    public class ProductController : Controller {
        private readonly ProductContext context;

        public ProductController(ProductContext context)
        {
            this.context = context;

            if (this.context.ProductItems.Count() == 0) {

                this.context.ProductItems.Add(new Product { ProductCode = "Product Code 1", Name = "Product Item 1", Quantity = 1, Price = new decimal(110.50) });
                this.context.SaveChanges();
            }
        }

        [HttpPost]
        public IActionResult Create([FromBody] Product item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            context.ProductItems.Add(item);
            context.SaveChanges();

            return CreatedAtRoute("GetProduct", new { id = item.ProductID }, item);

        }

        [HttpGet]
        public List<Product> GetAll()
        {
            return context.ProductItems.ToList();
        }

        [HttpGet("{id}", Name = "GetProduct")]
        public IActionResult GetById(int id)
        {
            var item = context.ProductItems.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Product item)
        {
            if (item == null || item.ProductID != id)
            {
                return BadRequest();
            }

            var product = context.ProductItems.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            product.ProductCode = item.ProductCode;
            product.Name = item.Name;
            product.Quantity = item.Quantity;
            product.Price = item.Price;

            context.ProductItems.Update(product);
            context.SaveChanges();

            return NoContent();


        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var item = context.ProductItems.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            context.ProductItems.Remove(item);
            context.SaveChanges();

            return NoContent();
        }


    }
}
